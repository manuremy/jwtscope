package com.astere.requestbody;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class JwtScopeArgumentResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(JwtScope.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        var scopeAnnotation = parameter.getParameterAnnotation(JwtScope.class);
        var request = webRequest.getNativeRequest(HttpServletRequest.class);
        return ((Optional)(request.getAttribute(JwtScope.ATTRIBUTE_OR_HEADER_NAME)))
                .orElseGet(() -> scopeAnnotation.value());

    }
}
