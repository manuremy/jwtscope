package com.astere.requestbody;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;


import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

@ControllerAdvice(assignableTypes = {JwtController.class})
public class JwtRequestBodyAdvice extends RequestBodyAdviceAdapter implements RequestBodyAdvice {
    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        var scope= inputMessage.getHeaders().toSingleValueMap()
                .entrySet()
                .stream()
                .filter(e -> e.getKey().equalsIgnoreCase(JwtScope.ATTRIBUTE_OR_HEADER_NAME))
                .findFirst()
                .map(Map.Entry::getValue)
                .or(() -> (body instanceof Map map) ?
                            Optional.ofNullable((String) map.get(JwtScope.BODY_MAP_KEY)) :
                            Optional.empty());

        var request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        request.setAttribute(JwtScope.ATTRIBUTE_OR_HEADER_NAME, scope);
        return body;
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return Arrays.stream(methodParameter.getMethod().getParameters())
                .anyMatch(p -> p.getAnnotation(JwtScope.class) != null);
    }
}
