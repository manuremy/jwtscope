package com.astere.requestbody;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface JwtScope {
    static String ATTRIBUTE_OR_HEADER_NAME= "X-JWT-SCOPE";
    static String BODY_MAP_KEY = "scope";
    String value() default "default";
}
