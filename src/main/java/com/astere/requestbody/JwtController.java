package com.astere.requestbody;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class JwtController {
    @PostMapping("/jwt")
    public ResponseEntity<Object> getJWT(@RequestBody Map<String, Object> body, @JwtScope("private") String scope) {
        return ResponseEntity.ok("Scope lu:%s".formatted(scope));
    }
    @PostMapping("/jwt_sans_scope")
    public ResponseEntity<Object> getJWTSansScope(@RequestBody Map<String, Object> body, HttpServletRequest request) {
        if (request.getAttribute(JwtScope.ATTRIBUTE_OR_HEADER_NAME)==null) {
            return ResponseEntity.ok("Sans Scope et sans attribut");
        }
        else
            return ResponseEntity.badRequest().build();
    }
}
